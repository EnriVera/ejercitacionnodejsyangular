import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import {SignInComponent} from './User/pages/sign-in/sign-in.component';
import {SignUpComponent} from './User/pages/sign-up/sign-up.component';
import {TareaComponent} from './Task/pages/tarea/tarea.component';
import {CuentaComponent} from './User/pages/cuenta/cuenta.component';

const routes: Routes = [
  { path: 'signin', component: SignInComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'tarea', component: TareaComponent},
  { path: 'cuenta', component: CuentaComponent},
  { path: '', redirectTo: '/tarea', pathMatch: 'full' },
  { path: '**', redirectTo: '/tarea', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
