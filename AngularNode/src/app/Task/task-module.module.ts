import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { TareaComponent } from './pages/tarea/tarea.component';
import {TaskComponent} from './components/task/task.component';
import {ModificarTaskComponent} from './components/modificar-task/modificar-task.component';
import {EliminarTaskComponent} from './components/eliminar-task/eliminar-task.component';
import {MaterialModuleModule} from '../material-module/material-module.module';
import { AgregarTaskComponent } from './components/agregar-task/agregar-task.component';





@NgModule({
  declarations: [ TareaComponent, TaskComponent, ModificarTaskComponent, EliminarTaskComponent, AgregarTaskComponent],
  imports: [
    CommonModule,
    MaterialModuleModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [ TareaComponent, TaskComponent, ModificarTaskComponent, EliminarTaskComponent, AgregarTaskComponent]
})
export class TaskModuleModule { }
