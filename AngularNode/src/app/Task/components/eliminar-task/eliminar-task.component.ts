import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskServiceService} from '../../task-service/task-service.service';

@Component({
  selector: 'app-eliminar-task',
  templateUrl: './eliminar-task.component.html',
  styleUrls: ['./eliminar-task.component.scss']
})
export class EliminarTaskComponent implements OnInit {
  @Input() idUser: String = ""
  @Input() idTask: String = ""
  @Output() close = new EventEmitter<string>();
  constructor( public  serviceTask: TaskServiceService) { }

  ngOnInit(): void {
    console.log(this.idTask)
  }

  EliminarTask(){
    this.serviceTask.DeleteTask(this.idTask, this.idUser).subscribe(
      value => this.close.emit(null),
      error => console.log('todo mal')
    )
  }
}
