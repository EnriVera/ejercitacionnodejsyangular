import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarTaskComponent } from './eliminar-task.component';

describe('EliminarTaskComponent', () => {
  let component: EliminarTaskComponent;
  let fixture: ComponentFixture<EliminarTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
