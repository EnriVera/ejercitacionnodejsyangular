import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Task} from '../../models/Task-Model';
import {TaskServiceService} from '../../task-service/task-service.service';
import {UserServiceService} from '../../../User/user-service/user-service.service';


@Component({
  selector: 'app-agregar-task',
  templateUrl: './agregar-task.component.html',
  styleUrls: ['./agregar-task.component.scss']
})
export class AgregarTaskComponent implements OnInit {

  task: Task = new Task();
  @Input() idUser: String = ""
  @Output() close = new EventEmitter<string>();
  selected = 'option2';
  constructor(public serviceTask: TaskServiceService, public serviceUser: UserServiceService) { }

  ngOnInit(): void {
  }

  AgregarTarea(){
    this.task.fecha_creacion = new Date(Date.now())
    this.task.user = null;
    if (this.task.titulo != undefined){
        this.serviceTask.PostTask(this.task).subscribe(
          value => {
            this.serviceUser.AgregarTaskEnUser(value.idTask, this.idUser).subscribe(
              value1 => {this.close.emit(null)},
              error => console.log(error)
            )
          },
          error => console.log(error)
        )
    }
  }
}
