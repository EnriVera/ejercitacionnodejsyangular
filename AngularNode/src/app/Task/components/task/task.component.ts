import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../models/Task-Model';
import { TaskServiceService } from '../../task-service/task-service.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {


  @Input() idUser: String = ""
  @Input() task: Task = new Task()
  @Output() close = new EventEmitter<string>();
  Modificar: boolean = false
  Eliminar: boolean = false
  constructor( public serviceTask: TaskServiceService) { }

  ngOnInit(): void {
    if(this.task.estados == undefined) this.task.estados = 0
  }


  Completado(){
    this.serviceTask.CompleteTask(this.task._id).subscribe(
      value => {},
      error => console.log(error)
    )
  }

}
