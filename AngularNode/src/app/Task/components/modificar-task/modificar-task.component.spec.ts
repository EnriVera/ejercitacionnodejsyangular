import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarTaskComponent } from './modificar-task.component';

describe('ModificarTaskComponent', () => {
  let component: ModificarTaskComponent;
  let fixture: ComponentFixture<ModificarTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
