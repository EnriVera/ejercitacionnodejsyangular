import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {Task} from '../../models/Task-Model';
import {TaskServiceService} from '../../task-service/task-service.service';

@Component({
  selector: 'app-modificar-task',
  templateUrl: './modificar-task.component.html',
  styleUrls: ['./modificar-task.component.scss']
})
export class ModificarTaskComponent implements OnInit {

  @Input() task: Task = new Task()
  @Output() close = new EventEmitter<string>();
  selected = 'option2';
  constructor( public servicetask: TaskServiceService ) { }

  ngOnInit(): void {
  }


  ModificarTarea(){
    console.log(this.task)
    if(this.task.titulo != undefined){
      this.servicetask.PutTask(this.task).subscribe(
        value => this.close.emit(null),
        error => console.log(error)
      )
    }
  }

}
