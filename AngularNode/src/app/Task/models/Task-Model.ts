export class Task {
  _id: String
  titulo: String
  descripcion: String
  fecha_creacion: Date
  fecha_inicio: Date
  fecha_finalizacion: Date
  completado: boolean
  estados: Number
  user: {}
}
