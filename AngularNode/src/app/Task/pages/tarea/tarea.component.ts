import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import {CookieService} from 'ngx-cookie-service';

import {TaskServiceService} from '../../task-service/task-service.service';
import {environment} from '../../../../environments/environment';
import {Task} from '../../models/Task-Model';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.scss']
})
export class TareaComponent implements OnInit {

  Agregartarea: boolean = false
  password: String =  environment.passwordEncrypt
  idUser: String
  task: Array<Task> = []
  habilitarTask: boolean = false;
  constructor(public  serviceTask: TaskServiceService, public cookie: CookieService) {
    const secion = this.cookie.get('secion')
    this.idUser = CryptoJS.AES.decrypt(secion, this.password).toString(CryptoJS.enc.Utf8);
  }

  ngOnInit(): void {
    this.TraerTaskInUser()
  }


  TraerTaskInUser(){
    this.habilitarTask = false
    this.serviceTask.GetTask(this.idUser).subscribe(
      value => {
        this.task = value.Task
        this.habilitarTask = true
      },
      error => console.log(error)
    )
  }

}
