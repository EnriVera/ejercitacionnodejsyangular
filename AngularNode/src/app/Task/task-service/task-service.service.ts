import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Task} from '../models/Task-Model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  public url: string;

  constructor( private http: HttpClient ) { this.url = environment.desarrollo }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  PostTask(task: Task): Observable<any>{
    return this.http.post(this.url+`apiTask/AddTask`, JSON.stringify(task), this.httpOptions)
  }

  GetTask(idTask: String): Observable<any>{
    return  this.http.get(this.url+`apiTask/GetTaskTheUser?id=${idTask}`)
  }

  PutTask(task: Task): Observable<any>{
    return this.http.put(this.url+`apiTask/ModificarTask`, JSON.stringify(task), this.httpOptions)
  }

  DeleteTask(idTask: String, idUser: String): Observable<any>{
    return this.http.delete(this.url+`apiTask/EliminarTask?idTask=${idTask}&idUser=${idUser}`)
  }

  CompleteTask(idTask: String): Observable<any>{
    return this.http.patch(this.url+`apiTask/CompletadoTask?id=${idTask}`, JSON)
  }
}
