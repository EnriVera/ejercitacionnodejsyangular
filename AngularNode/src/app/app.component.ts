import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'AngularNode';
  habilitar: boolean = false;
  sidebar: boolean = false

  constructor(public cookie: CookieService, public  router: Router) {
    const idUser = this.cookie.get('secion')
    idUser != "" ? router.navigate(['/tarea']) : router.navigate(['/signin'])

    router.events.forEach((event) => {
      if(event instanceof NavigationEnd) {
        event.url == '/signin' || event.url == '/signup' ? this.habilitar = true : this.habilitar = false;
      }
    });
  }

  ngOnInit(): void {
  }
}
