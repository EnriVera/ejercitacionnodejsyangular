import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../models/User-model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  public url: string;

  constructor( private http: HttpClient ) { this.url = environment.desarrollo }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  UserSignUp(user: User): Observable<any>{
    return this.http.post(this.url+`apiUser/signup`, JSON.stringify(user), this.httpOptions)
  }

  UserSignIn(user: User): Observable<any>{
    //return this.http.get(this.url+`apiUser/signin?email:${user.email}&password=${user.password}`, this.httpOptions)
    return this.http.post(this.url+`apiUser/signin`, JSON.stringify(user), this.httpOptions)
  }

  AgregarTaskEnUser(idTask: String, idUser: String):Observable<any>{
    return this.http.post(this.url+`apiUser/AddTaskInUser?idTask=${idTask}&idUser=${idUser}`, JSON)
  }

  MostrarUser(idUser: String):Observable<any>{
    return this.http.get(this.url+`apiUser/MostrarUser?id=${idUser}`)
  }

  ModificarUser(user: User):Observable<any>{
    return this.http.patch(this.url+`apiUser/ModificarUser`, JSON.stringify(user), this.httpOptions)
  }
}
