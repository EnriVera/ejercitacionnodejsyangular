import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {environment} from '../../../../environments/environment';
import * as CryptoJS from 'crypto-js';
import {UserServiceService} from '../../user-service/user-service.service';
import {User} from '../../models/User-model';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.scss']
})
export class CuentaComponent implements OnInit {

  password: String =  environment.passwordEncrypt
  idUser: String
  user: User = new User()
  constructor(public serviceUser: UserServiceService, public cookie: CookieService) {
    const secion = this.cookie.get('secion')
    this.idUser = CryptoJS.AES.decrypt(secion, this.password).toString(CryptoJS.enc.Utf8);
  }

  ngOnInit(): void {
    this.serviceUser.MostrarUser(this.idUser).subscribe(
      value => {
        this.user = value.user},
      error => console.log(error)
    )
  }

  GuardarDatos(){
    if(this.user.nombre != undefined || this.user.nombre != ""){
      this.serviceUser.ModificarUser(this.user).subscribe(
        value => this.user = value.user,
        error => console.log(error)
      )
    }
  }
}
