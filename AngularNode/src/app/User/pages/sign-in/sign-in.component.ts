import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User-model';
import {UserServiceService} from '../../user-service/user-service.service';
import {CookieService} from 'ngx-cookie-service';
import * as CryptoJS from 'crypto-js';
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  User: User = new User()
  password: String =  environment.passwordEncrypt
  constructor(public serviceUser: UserServiceService, public cookie: CookieService, public router: Router) { }

  ngOnInit(): void {
  }

  iniciarSecion(){
    this.serviceUser.UserSignIn(this.User).subscribe(
      value => {
        this.cookie.delete('secion')
        this.cookie.set('secion', CryptoJS.AES.encrypt(value.message[0], this.password).toString())

        this.router.navigate(['/tarea'])
      },
      error => {console.log(error)}
    )
  }
}
