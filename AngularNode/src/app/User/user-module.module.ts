import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { CuentaComponent } from './pages/cuenta/cuenta.component';




@NgModule({
  declarations: [
    SignInComponent, SignUpComponent, CuentaComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CommonModule
  ],
  exports: [SignInComponent, SignUpComponent, CuentaComponent]
})
export class UserModuleModule { }
