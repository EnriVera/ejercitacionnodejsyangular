// angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// service
import {CookieService} from 'ngx-cookie-service';

// component
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {UserModuleModule} from './User/user-module.module';
import {TaskModuleModule} from './Task/task-module.module';
import {MaterialModuleModule} from './material-module/material-module.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    UserModuleModule,
    TaskModuleModule,
    MaterialModuleModule
  ],
  providers: [ CookieService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
