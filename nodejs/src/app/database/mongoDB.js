const mongoose = require('mongoose')

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/EjercitacionNodejs', {useUnifiedTopology: true, useNewUrlParser: true})
        .then(() => console.log('Base de datos conectodo'))
        .catch(err => console.log('Base de datos fallada'))