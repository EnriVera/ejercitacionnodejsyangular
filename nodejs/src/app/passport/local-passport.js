const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy; 
const User = require('../model/DTO/userDTO');

passport.serializeUser((user, done) => {
    done(null, user.id);
})

passport.deserializeUser(async (id, done) => {
    const user = await User.findById(id);
    done(null, user)
})

passport.use('local_signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    await User.find({ email: email }, (err, user) => {

        if(user.length != 0) {
            return done(null, false, req.flash('signupMessage', 'El correo ya existe'))
            //return done(null, false, {message: 'El correo ya existe'})
        }
        else{
            const user = new User();
            user.nombre = req.body.nombre;
            user.apellido = req.body.apellido;
            user.password = user.encryptPasswork(req.body.password);
            user.email = req.body.email;
            user.save();

            req.flash('IdUser', user.id)
            done(null, user)
        }

    })
}))


passport.use('local_signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    await User.find({ email: email}, (err, dataUser) => {
        if(dataUser.length == 0) {
            console.log(dataUser.length)
            return done(null, false, req.flash('signupMessage', 'El correo no existe'))
            //return done(null, false, {message: 'El correo ya existe'})
        }
        else{
            const user = new User();
            user.nombre = dataUser[0].nombre
            user.apellido = dataUser[0].apellido
            user.email = dataUser[0].email
            user.description = dataUser[0].description

            if(!dataUser[0].validarPasswork(password)){ return done(null, false, {message: 'El correo y contraseña no existe'}) }

            req.flash('IdUser', dataUser[0].id)

            done(null, user)
        }
    })
}))