const express = require("express");
const router = express.Router();
const passport = require("passport");

// Controlladores
const UserController = require("../controller/userController");

router.get("/", (req, res, next) => {
  res.status(200).send("<h1>Welcome</h1>");
});

router.get('/loginfail', function(req, res){
    return res.send({message: 'todo mall'})
    //res.json(403, {message: 'Invalid username/password'});
});


router.post('/signup',
    passport.authenticate('local_signup', {
    failureRedirect: "/loginfail",
    passReqToCallback: true,}), (req, res) => {res.send({message: res.locals.mensajeRegistro = req.flash('IdUser')})}
)

router.post(
  "/signin",
  passport.authenticate("local_signin", {
    failureRedirect: "/loginfail",
    passReqToCallback: true,}), (req, res) => {res.send({message: res.locals.mensajeRegistro = req.flash('IdUser')})}
);

router.post("/AddTaskInUser", UserController.AgregarTaskInUser);

router.get("/MostrarUser", UserController.MostrarUser);

router.patch("/ModificarUser", UserController.ModificarUser);

module.exports = router;
