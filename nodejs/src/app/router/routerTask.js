const express = require("express");
const router = express.Router();

// Controlladores
const TaskController = require("../controller/taskController");

router.post('/AddTask', TaskController.AgregarTask)

router.get('/GetTaskTheUser', TaskController.MostrarTaskTheUser)

router.put('/ModificarTask', TaskController.ModificarTask)

router.delete('/EliminarTask', TaskController.EliminarTask)

router.patch('/CompletadoTask', TaskController.CompletadoTask)

module.exports = router;
