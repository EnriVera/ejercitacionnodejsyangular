const TaskRepository = require('../model/Repository/taskRepository')

var TaskController = {

    AgregarTask: (req, res) => {
        return TaskRepository.AgregarTask(req, res)
    },

    MostrarTaskTheUser: (req, res) => {
        return TaskRepository.MostrarTaskTheUser(req, res)
    },

    ModificarTask: (req, res) => {
        return TaskRepository.ModificarTask(req, res)
    },

    EliminarTask: (req, res) => {
        return TaskRepository.EliminarTask(req, res)
    },

    CompletadoTask: (req, res) => {
        return TaskRepository.CompleteTask(req, res)
    },
}

module.exports = TaskController;