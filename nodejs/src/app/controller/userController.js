const UserRepository = require('../model/Repository/userRepository')

var UserController = {

    AgregarTaskInUser: (req, res) => {
        return UserRepository.GuardartaskInUser(req, res)
    },

    MostrarUser: (req, res) => {
        return UserRepository.MostrarUser(req, res)
    },
    
    ModificarUser: (req, res) => {
        return UserRepository.ModificarUser(req, res)
    }
}

module.exports = UserController;