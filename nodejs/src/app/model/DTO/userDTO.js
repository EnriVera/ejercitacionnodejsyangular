const mongoose = require('mongoose')
const bcrypt = require('bcrypt-nodejs')
const { Schema } = mongoose

const userSchema  = Schema({
    _id: String,
    nombre: {type: String, required: true},
    apellido: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    description: String, 
    task: []
})

userSchema.methods.encryptPasswork = (password) =>{
    return bcrypt.hashSync(password, bcrypt.genSaltSync(50))
}

userSchema.methods.validarPasswork = function (password) {
    return bcrypt.compareSync(password, this.password)
}

module.exports = mongoose.model('users', userSchema);