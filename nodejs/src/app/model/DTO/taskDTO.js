const mongoose = require('mongoose')
const { Schema } = mongoose

const taskSchema  = Schema({
    titulo: {type: String, required: true, maxWidth: 100},
    descripcion: {type: String, maxWidth: 300},
    fecha_creacion: {type: Date, required: true},
    fecha_inicio: Date,
    fecha_finalizacion: Date,
    estados: Number,
    completado: Boolean,
    user: {}
})

module.exports = mongoose.model('tasks', taskSchema);