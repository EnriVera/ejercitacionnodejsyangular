const TaskDTO = require('../DTO/taskDTO')
const UserDTO = require('../DTO/userDTO')

var taskRepository = {
    
    
    AgregarTask: function (req, res) {
        const body = req.body
        const task = new TaskDTO()

        task.titulo = body.titulo;
        task.descripcion = body.descripcion
        task.fecha_creacion = new Date(body.fecha_creacion)
        task.fecha_inicio = body.fecha_inicio
        task.fecha_finalizacion = body.fecha_finalizacion
        task.estados = body.estados
        task.completado = false
        task.user = body.user

        task.save((err, data) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!data) return res.status(404).send({message: 'No se a podido guardar'})
    
            return res.status(200).send({idTask: data.id})
        })
    },

    MostrarTaskTheUser: (req, res) => {
        UserDTO.findById(req.query.id, (err, dataUser) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(dataUser.task.length == 0) return res.status(404).send({message: 'No hay Tareas'})

            return Buscartask(dataUser, res).catch(error=>console.log(error))
        })
    },

    ModificarTask: function (req, res) {
        const body = req.body
        let task = TaskDTO.findById(body._id, (err, dataTask) => {
            if(err) return res.status(500).send({message: 'Error en el documento '})
    
            if(!dataTask) return res.status(404).send({message: 'No existe el id'})

            return dataTask
        }).then(function(dataTask) {
            dataTask.titulo = body.titulo;
            dataTask.descripcion = body.descripcion
            dataTask.fecha_creacion = new Date(body.fecha_creacion)
            dataTask.fecha_inicio = body.fecha_inicio
            dataTask.fecha_finalizacion = body.fecha_finalizacion
            dataTask.estados = body.estados
            dataTask.user = body.user

            dataTask.save((err, data) => {
                if(err) return res.status(500).send({message: 'Error en el documento'})
    
                if(!data) return res.status(404).send({message: 'No se a podido guardar'})
    
                return res.status(200).send({message: 'Todo correcto'})
            })
        })
    },

    EliminarTask: (req, res) => {
        var idTask = req.query.idTask;
        var idUser = req.query.idUser;


        TaskDTO.findByIdAndDelete(idTask, (err, task) =>{
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!task) return res.status(404).send({message: 'No existe el id'})

            return UserDTO.findById(idUser, (err, user) => {
                if(err) return res.status(500).send({message: 'Error en el documento'})
    
                if(!user) return res.status(404).send({message: 'No existe el id'})
                console.log("Holllallaaaa")
                for (const key in user.task) {
                    if(user.task[key] === idTask){
                        var i = user.task.indexOf( user.task[key] );
                        user.task.splice( i, 1 )
                    }
                }

                return user.save((err, data) => {
                    if(err) return res.status(500).send({message: 'Error en el documento'})
        
                    if(!data) return res.status(404).send({message: 'No se a podido guardar'})
        
                    return res.status(200).send({message: 'Todo correcto'})
                })
            })
        })
    },

    CompleteTask: (req, res) => {
        var idTask = req.query.id;

        TaskDTO.findById(idTask, (err, task) =>{
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!task) return res.status(404).send({message: 'No existe el id'})

            task.completado = !task.completado

            task.save((err, data) => {
                if(err) return res.status(500).send({message: 'Error en el documento'})
    
                if(!data) return res.status(404).send({message: 'No se a podido guardar'})
    
                return res.status(200).send({message: 'Todo correcto'})
            })
        })
    }
}


async function Buscartask(dataUser, res) {
    let task = new Array()
    for (const key in dataUser.task) {
        var valor = await TaskDTO.findById(dataUser.task[key], (err, dataTask) => {
            if(err) return res.status(500).send({message: 'Error en el documento '})
    
            if(!dataTask) return res.status(404).send({message: 'No existe el id'})

            return dataTask
        })

        if(valor.length === 0) return valor
        
        task.push(valor)
    }
    return res.status(200).send({Task: task})
}

module.exports = taskRepository