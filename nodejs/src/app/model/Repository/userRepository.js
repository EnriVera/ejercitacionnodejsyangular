const UserDTO = require('../DTO/userDTO')
const TaskDTO = require('../DTO/taskDTO')
const { use } = require('passport')

var UserRepository = {


    GuardartaskInUser: (req, res) => {
        const idTask = req.query.idTask
        const idUser = req.query.idUser
        
        TaskDTO.findById(idTask, (err, dataTask) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!dataTask) return res.status(404).send({message: 'No existe el id'})

            return UserDTO.findById(idUser, (err, dataUser) => {
                dataUser.task.push(dataTask.id)
                return dataUser.save((err, userStore) => {
                    if(err) return res.status(500).send({message: 'Error en el documento'})
            
                    if(!userStore) return res.status(404).send({message: 'no se a podido guardar'})
            
                    return res.status(200).send({user: userStore})
                })
            })

        })
    },

    MostrarUser: (req, res) => {
        const iduser = req.query.id

        UserDTO.findById(iduser, (err, dataUser) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
            
            if(!dataUser) return res.status(404).send({message: 'El id es invalida'})

            const user = UserDTO()

            user._id = dataUser.id
            user.nombre = dataUser.nombre
            user.apellido = dataUser.apellido
            user.description = dataUser.description
            user.email = dataUser.email

            return res.status(200).send({user: user})
        })
    },

    ModificarUser: (req, res) => {
        const body = req.body
        
        UserDTO.findById(body._id, (err, dataUser) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
            
            if(!dataUser) return res.status(404).send({message: 'El id es invalida'})

            dataUser.nombre = body.nombre
            dataUser.apellido = body.apellido
            dataUser.description = body.description

            return dataUser.save((err, data) => {
                if(err) return res.status(500).send({message: 'Error en el documento'})
                
                if(!data) return res.status(404).send({message: 'no se a podido guardar'})
                
                return res.status(200).send({user: data})
            })
        })
    }

    /*GuardarUser: (req, res) => {
        var user = new UserDTO()
        var params = req.body
    
        person.nombre_person = params.nombre
        person.apellido_person = params.apellido
        person.fecha_nace_person = params.fechaNasimiento
        person.email_person = params.email
        person.passwork_person = params.password
        person.descripcion_person = params.descripcion
        //person.task_person = params.task
    
    
        person.save((err, personStore) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!personStore) return res.status(404).send({message: 'no se a podido guardar'})
    
            return res.status(200).send({person: personStore})
        })
    },


    MostrarPersonID: (req, res) => {
        var personID = req.query.id;

        PersonDTO.findById(personID, (err, person) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!person) return res.status(404).send({message: 'No existe el id'})
    
            return res.status(200).send({Person: person})
        })
    },


    EliminarPerson: (req, res) => {
        var idPerson = req.query.id;

        PersonDTO.findByIdAndDelete(idPerson, (err, person) =>{
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!person) return res.status(404).send({message: 'No existe el id'})
    
            return res.status(200).send({message: 'Se a eliminado la persona'})
        })
    },

    MostrarPerson: (req, res) => {
        PersonDTO.find({}).exec((err, person) => {
            if(err) return res.status(500).send({message: 'Error en el documento'})
    
            if(!person) return res.status(404).send({message: 'No hay datos'})
    
            return res.status(200).send({person})
        })
    }*/
}

module.exports = UserRepository