const express = require('express')
const morgan = require('morgan')
const passport = require('passport')
const session = require('express-session')
const bodyParser = require('body-parser')
const cors = require('cors')
const flash = require('connect-flash')
const app = express();
require('./app/database/mongoDB')
require('./app/passport/local-passport')

// rutas
const routerUser = require('./app/router/routerUser')
const routerTask = require('./app/router/routerTask')

// configuration
app.set('port', process.env.PORT || 3000); 


// middleware
app.use(cors())
app.use(morgan('dev'))
app.use(express.urlencoded({extended: false}))
app.use(bodyParser.json());
app.use(require('serve-static')(__dirname + '/../../public'));
app.use(require('cookie-parser')());
app.use(session({
    secret:'secion',
    resave: false,
    saveUninitialized: false
}))
app.use(flash())
app.use(passport.initialize())
app.use(passport.session())

app.use((req, res, next) => {
    app.locals.signupMessage = req.flash('signupMessage')
    next()
})


app.listen(app.get('port'), ()=>{ console.log(`Tu puerto es ${app.get('port')}`) })

app.use('/apiUser', routerUser)
app.use('/apiTask', routerTask)